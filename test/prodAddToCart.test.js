let prodAddToCart = (function (beforeAll, describe, afterAll, test) {
    let page;
    let browser;
    const puppeteer = require('puppeteer');
    const APP = "https://www.glassesusa.com/shiny-blackblue-extra-large/oakley-oo9102-holbrook/46-000638.html?promo=lenses70";
    const width = 1920;
    const height = 1080;

    beforeAll(async () => {
        browser = await puppeteer.launch({
            headless: false,
            slowMo: 80,
            args: [`--window-size=${width},${height}`]
        });
        page = await browser.newPage();
        await page.setViewport({ width, height });
    });

    describe('addToCart', () => {
        test('Page loading', async () => {
            await page.goto(APP);
        }, 10000);

        test('Close widget with stocks', async () => {
            await page.waitForXPath('//*[@id="appDynamic"]');
            await page.click('.dyWelcomePopup__noThanks');
        }, 10000);

        test('Choice of prescription', async () => {
            await page.waitForSelector('[data-test-name="useGlassesFor"]');
            await page.click('[data-test-name="useGlassesFor"] [data-test-value="Non-prescription"]');
        }, 10000);

        test('Choose lens package', async () => {
            await page
                .waitForTimeout(1000)
                .then(() => {
                    page.waitForSelector('[data-test-name="lensPackage"]');
                });
            await page.click('[data-test-name="lensPackage"] [data-test-value="Lite and Thin (1.61 High Index)"]');
        }, 10000);

        test('Choose lens type', async () => {
            await page.waitForSelector('[data-test-name="lensColor"]');
            await page.click('[data-test-name="lensColor"] [data-test-value="Polarized"] [data-test-value="Gray"]');
        }, 10000);

        test('Add product to cart', async () => {
            await page.waitForSelector('[data-test-name="addToCart"]');
            await page.click('[data-test-name="addToCart"] button');
        }, 10000);

        test('Check adding to cart', async () => {
            await page.waitForSelector('[data-test-name="cartItem"]');
        }, 10000);
    });

    afterAll(() => {
        browser.close();
    });
}(beforeAll, describe, afterAll, test));